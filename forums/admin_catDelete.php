<?php
////////////////////////
//
//  admin_catDelete.php
//  Included by module.php
//  Admins only.
//  Deletes categories.
////////////////////////


if(((isset($_SESSION['forum_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['forum_' . $moduleNumber . '_admin_2'])) && ($_SESSION['forum_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['forum_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ $catNumber = $_GET['p']; }else{ session_destroy(); die("Error!"); }

//Query this category to get name to double check that user clicked on the right thing
$query = 'SELECT name FROM forum_' . $moduleNumber . '_categories WHERE number=' . $catNumber;
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                $catName = $row->name;
        }
}
unset($query); unset($row); unset($result);


if((isset($_GET['v'])) && ($_GET['v']==1)){
	//if we have confirmed to delete the category

	//get post numbers for thread starting posts in this category
        $query = 'SELECT number FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $catNumber;
        if($result = mysqli_query($link, $query)){
		while($row = mysqli_fetch_object($result)){
			//delete replies
                	$subQuery = 'DELETE FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $row->number;
		        if(!$subResult = mysqli_query($link, $subQuery)){
		            	die("Error!");
		        } //end sub if
		        unset($subQuery); unset($subResult);
		} //end normal while
        } //end normal if
        unset($query); unset($result);

	//now delete the thread starting posts
	$query = 'DELETE FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $catNumber;
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($result);

	//delete the category
	$query = 'DELETE FROM forum_' . $moduleNumber . '_categories WHERE number=' . $catNumber;
	if(!$result = mysqli_query($link, $query)){
        	die("Error!");
	}
	unset($query); unset($result);

include('.' . $modulePath . 'admin_header.php');
?>
<h1>Deleted <?php echo $catName; ?>!</h1>
<?php
include('.' . $modulePath . 'admin_footer.php');
}

mysqli_close($link);

if((!isset($_GET['v'])) || ($_GET['v']!=1)){ //if not confirmed
include('.' . $modulePath . 'admin_header.php');
?>
<h1>Are you sure you want to delete category <?php echo "(" . $catNumber . ") " . $catName; ?>?</h1><h2>All posts will be deleted too!<br>You cannot undo this!</h2>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=6&p=<?php echo $catNumber; ?>&v=1">Yes, Continue.</a>
<?php
include('.' . $modulePath . 'admin_footer.php');
} //end if not confirmed
} //if you are an admin of the website
?>
