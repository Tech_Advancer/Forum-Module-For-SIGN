<?php
//////////////////////////////
//
//  activateAccount.php
//  Included by module.php
//  Verifies user's email address.
/////////////////////////////

$aac = $_GET['aac'];

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'UPDATE shared_users SET accountActivationCode=NULL WHERE accountActivationCode=?';
$query = mysqli_real_escape_string($link, $query);
$stmt = mysqli_stmt_init($link);

if(mysqli_stmt_prepare($stmt, $query)){
        mysqli_stmt_bind_param($stmt, "s" , $aac);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        unset($stmt); unset($query);
	$message='Your account has been activated. You can now login at any time!<br><a href="./index.php?m=' . $moduleNumber . '&e=2">Login</a>';
}else{
die("Error!");
}

include($root . $modulePath . $themePath . "header.html");
include($root . $modulePath . $themePath . "message.html");
include($root . $modulePath . $themePath . "footer.html");

?>
