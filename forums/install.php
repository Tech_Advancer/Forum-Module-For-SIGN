<?php
//////////////////////////
//
//  install.php
//  Installs the forum module.
//  Included by (root)/install.php
//    and the admin panel.
//  Requires $linkBack to be set!
//////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

if((isset($_POST['forumName'])) && (isset($_POST['catName'])) && (isset($_POST['catDes']))){
//if the form has been submitted

	$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

	$moduleName = db_safe($_POST['forumName'], $link);
	$moduleLocation = db_safe($_POST['moduleLocation'], $link);
	$catName = db_safe($_POST['catName'], $link);
	$catDes = db_safe($_POST['catDes'], $link);

        $query = 'INSERT INTO shared_installations (name,location) VALUES (?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ss", $moduleName, $moduleLocation);
                mysqli_stmt_execute($stmt);
		$moduleNumber = mysqli_insert_id($link);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }

	$dbLoadFirst = -1;

	//Check if loadFirst has been set in db below:
	$query = 'SELECT intValue FROM shared_settings WHERE name="loadFirst"';

	if($result = mysqli_query($link, $query)){
	        while($row = mysqli_fetch_object($result)){
			$dbLoadFirst = $row->intValue;
	        }
	}
	unset($query); unset($row); unset($result);

	if($dbLoadFirst<0){ //if loadFirst isn't set, set this installation to loadFirst
                $query = 'UPDATE shared_settings SET intValue=' . $moduleNumber . ' WHERE name="loadFirst"';
                if(!$result = mysqli_query($link, $query)){
                        die("Error!<br> $query <br>" . mysqli_error($link));
                }
                unset($query); unset($row); unset($result);

		$query = 'UPDATE shared_users SET loadFirst=' . $moduleNumber . ' WHERE loadFirst=-1';
                if(!$result = mysqli_query($link, $query)){
                        die("Error!");
                }
                unset($query); unset($row); unset($result);
        }

	$query = 'CREATE TABLE forum_' . $moduleNumber . '_categories(number bigint(20) NOT NULL AUTO_INCREMENT,name longtext NOT NULL,description longtext,PRIMARY KEY(number))';
        if(!$result = mysqli_query($link, $query)){
        	die("Error!");
        }
        unset($query); unset($row); unset($result);

	$query = 'INSERT INTO forum_' . $moduleNumber . '_categories (name,description) VALUES (?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ss", $catName, $catDes);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }

	$query = 'CREATE TABLE forum_' . $moduleNumber . '_posts(number bigint(20) NOT NULL AUTO_INCREMENT,name longtext NOT NULL,content longtext,linkNumber bigint(20),author bigint(20) NOT NULL,date text,isReply tinyint(1) NOT NULL,PRIMARY KEY(number),FOREIGN KEY(author) REFERENCES shared_users(number))';
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($row); unset($result);

	$query = 'CREATE TABLE forum_' . $moduleNumber . '_settings(name text NOT NULL,intValue int(11),textValue longtext)';
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($row); unset($result);

	$query = 'INSERT INTO forum_' . $moduleNumber . '_settings (name,textValue) VALUES("links","<b>Links:</b><br><a href=\"./index.php?m='  . $moduleNumber . '\">Home</a><br>")';
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($row); unset($result);

	$query = 'INSERT INTO forum_' . $moduleNumber . '_settings (name,textValue) VALUES("theme","basic")';
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($row); unset($result);

	$query = 'CREATE TABLE forum_' . $moduleNumber . '_users(number bigint(20) NOT NULL,rank int(11),publicRank text,FOREIGN KEY(number) REFERENCES shared_users(number))';
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($row); unset($result);


	$query = 'SELECT number FROM shared_users';

	if($result = mysqli_query($link, $query)){
	        while($row = mysqli_fetch_object($result)){
			if($row->number != $_SESSION['user_number']){
				$subQuery = 'INSERT INTO forum_' . $moduleNumber . '_users(number,rank,publicRank) VALUES(' . $row->number . ',0,"User")';
                                        if(!$subResult = mysqli_query($link, $subQuery)){
                                                die("Error!");
                                        }
                                unset($subQuery); unset($subRow); unset($subResult);
			}else{
				$subQuery = 'INSERT INTO forum_' . $moduleNumber . '_users(number,rank,publicRank) VALUES(' . $row->number . ',3,"Admin")';
                                        if(!$subResult = mysqli_query($link, $subQuery)){
                                                die("Error!");
                                        }
                                unset($subQuery); unset($subRow); unset($subResult);
			}
        	}
	}
	unset($query); unset($row); unset($result);
	mysqli_close($link);

	if(isset($_POST['postCheck'])){ //if we are coming from the SIGN installer
        session_destroy();
?>
<h1>Done!</h1>
<p>Both SIGN and your new forum module has been installed!<br>
<a href="./index.php">Click Here</a> to see it!</p>
<?php }else{ // if not in SIGN installer
?>
<h1>Done!</h1>
<a href="./index.php?m=<?php echo $moduleNumber; ?>">Click Here</a> to see the new module or
 <a href="./index.php?m=<?php echo $moduleNumber; ?>&a=1">Click Here</a> to go to its admin panel.
<?php
} //end if not in SIGN installer
}else{
//if the form hasn't been submitted yet:
?>

<h1>Install Forum Module</h1>

<?php
if((isset($message)) && (strlen($message)>0)){
?>
<div style="width: 95%; margin: auto; border: solid 3px red; color: red; font-size: 130%; text-align: center;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<div style="width: 95%; margin: auto;">
<form action="<?php echo $linkBack; ?>" method="POST" autocomplete="off">
<label>Module Name: <input type="text" name="forumName" value="My Forums"></label><br>

<?php
//if we need data from the SIGN installer:
if( (isset($_POST['adminUN'])) && (isset($_POST['adminNum'])) && (isset($_POST['dbURL'])) && (isset($_POST['dbName'])) &&
(isset($_POST['dbUN'])) && (isset($_POST['dbPW'])) && (isset($_POST['hash']))){
?>
<input type="hidden" name="adminUN" value="<?php echo $_POST['adminUN']; ?>">
<input type="hidden" name="adminNum" value="<?php echo $_POST['adminNum']; ?>">
<input type="hidden" name="dbURL" value="<?php echo $_POST['dbURL']; ?>">
<input type="hidden" name="dbName" value="<?php echo $_POST['dbName']; ?>">
<input type="hidden" name="dbUN" value="<?php echo $_POST['dbUN']; ?>">
<input type="hidden" name="dbPW" value="<?php echo $_POST['dbPW']; ?>">
<input type="hidden" name="hash" value="<?php echo $_POST['hash']; ?>">
<input type="hidden" name="postCheck" value="1">
<?php } ?>

<input type="hidden" name="moduleLocation" value="<?php echo $moduleLocation; ?>">
<label>Category Name: <input type="text" name="catName" value="My First Category"></label><br><br>
<label>Category Description: <br><textarea name="catDes">Category made by the installer</textarea></label><br><br>
<br><br>
<input type="submit" value="Install">
</form>
</div>

<?php
} //end if the form hasn't been submitted yet
} //end check if user is admin
?>
