<?php
////////////////////////
//
//  admin_catCreate.php
//  Included by module.php
//  Makes categories for the
//  forum module.
////////////////////////


if(((isset($_SESSION['forum_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['forum_' . $moduleNumber . '_admin_2'])) && ($_SESSION['forum_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['forum_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_POST['catName'])) && (isset($_POST['catDes']))){
//if form was submitted

	$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

        $catName = db_safe($_POST['catName'], $link);
        $catDes = db_safe($_POST['catDes'], $link);

        $query = 'INSERT INTO forum_' . $moduleNumber . '_categories (name,description) VALUES (?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ss", $catName, $catDes);
                mysqli_stmt_execute($stmt);
                $catNumber = mysqli_insert_id($link);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
	mysqli_close($link);

	include('.' . $modulePath . 'admin_header.php');
?>
<h1>Category Created!</h1>
<p><a href="./index.php?m=<?php echo $moduleNumber; ?>&a=4&p=<?php echo $catNumber; ?>">Click Here to edit</a> or
<a href="./index.php?m=<?php echo $moduleNumber; ?>&cat=<?php echo $catNumber; ?>">click here to view</a> your new category!</p>
<?php
	include('.' . $modulePath . 'admin_footer.php');
}else{
//if form hasn't been submitted
	include('.' . $modulePath . 'admin_header.php');
?>

<h1>Create a New Category for <?php echo $moduleName; ?></h1>

<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=5" method="POST">
<label>Category Name: <input type="text" name="catName"></label><br><br>
<label>Category Description:<br>
<textarea name="catDes"></textarea></label><br><br>
<input type="submit" value="Create">
</form>

<?php
	include('.' . $modulePath . 'admin_footer.php');
} // end if the form has been submitted
} //if you are an admin of the website

?>
