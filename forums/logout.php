<?php
/////////////////////////////
//
//  logout.php
//  A whole different page was
//    made just for the logout
//    to show that special
//    behaviors could be made
//    for just logging out.
////////////////////////////


unset($_SESSION['user_number']);
unset($_SESSION['user_name']);
unset($_SESSION['load_first']);
unset($_SESSION['time_zone']);
session_destroy();  //remember to unset cookies later!
$_SESSION['user_number']=-1;
$_SESSION['user_name']='Guest';

$message='You have been logged out.<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>';;
include($root . $modulePath . $themePath . "header.html");
include($root . $modulePath . $themePath . "message.html");
include($root . $modulePath . $themePath . "footer.html");
?>
