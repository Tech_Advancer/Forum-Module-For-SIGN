<?php
//////////////////////////////
//
// login.php
// Included by module.php
//////////////////////////////

$message='';

if($_SESSION['user_number']!=-1){ //if we are somehow already logged in
	include($root . $modulePath . $themePath . "header.html");
	$message.='You are already logged in!<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>'; //change this later
	include($root . $modulePath . $themePath . "message.html");
	include($root . $modulePath . $themePath . "footer.html");

}else{
	if((isset($_POST['submit'])) && (isset($_POST['userName'])) && (isset($_POST['password']))){ //if we are logging in

 	$link = db_connect($database_url,$database_username,$database_password,$database_name);

	$userName = db_safe($_POST['userName'], $link);
        $password = $_POST['password'];
        if(isset($_POST['stay'])){ $stay=true; }else{ $stay=false; }

        $query = "SELECT number,password,loadFirst,timeZone,accountActivationCode,ipList,banYear,banMonth,banDay,banReason,uniqueID,sharedAdmin FROM shared_users WHERE username=?";
        $stmt = mysqli_stmt_init($link);

        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "s", $userName);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_store_result($stmt);
                mysqli_stmt_bind_result($stmt, $dbNumber, $dbPassword, $dbLoadFirst, $dbTimeZone, $dbAAC, $dbIpList, $dbBanYear, $dbBanMonth, $dbBanDay, $dbBanReason, $dbUID, $dbSharedAdmin);
                mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		unset($query);
		//set session variables and check if password needs updating
		if(password_verify($password, $dbPassword)){
			//if(strlen($dbBan*)>0)
			if(strlen($dbAAC)==0){ //if user's email has been verified
				if((strlen($dbBanYear)==0) && (strlen($dbBanMonth)==0) && (strlen($dbBanDay)==0)){//check if user hasn't been banned
					$message.='Login Successful!<br><a href="./index.php?m=' . $moduleNumber . '">Continue</a>'; //change this later for language packs
					if(!password_needs_rehash($dbPassword, PASSWORD_DEFAULT, $hashOptions['options'])){
						unset($password); unset($dbPassword); //unset this BEFORE including any more of the page!
						$_SESSION['user_number'] = $dbNumber;
						$_SESSION['user_name'] = $userName;
						$_SESSION['load_first'] = $dbLoadFirst;
						$_SESSION['time_zone'] = $dbTimeZone;
						$_SESSION['is_admin_0'] = $dbSharedAdmin;

						//Update Last Login below:
						$query = 'UPDATE shared_users SET lastLogin="' . date("Y F j") . '" WHERE number=' . $dbNumber;
                                                $stmt = mysqli_stmt_init($link);

                                                if(mysqli_stmt_prepare($stmt, $query)){
                                                        mysqli_stmt_execute($stmt);
                                                        mysqli_stmt_close($stmt);
                                                        unset($query);
                                                }else{
                                                        die("Error!");
                                                }
						//Update Last Login Above:

						unset($link);
						include($root . $modulePath . $themePath . "header.html");
						include($root . $modulePath . $themePath . "message.html");
						include($root . $modulePath . $themePath . "footer.html");
					}else{
						$query = "UPDATE shared_users SET password='" . password_hash($password, PASSWORD_DEFAULT, $hashOptions['options']) . "' WHERE number=" . $dbNumber;
						$stmt = mysqli_stmt_init($link);
						if(mysqli_stmt_prepare($stmt, $query)){
							mysqli_stmt_execute($stmt);
						}else{ //if prepare fails:
							die("Error Loading Website! Please relog!");
						}
						unset($link); unset($password); unset($dbPassword);
						include($root . $modulePath . $themePath . "header.html");
		                                include($root . $modulePath . $themePath . "message.html");
		                                include($root . $modulePath . $themePath . "footer.html");
					}
				}else{ //if user has been banned
				unset($password); unset($dbPassword);
					//check to see if user should be unbanned now.
					if(date("Y")>$dbBanYear){
						$query = "UPDATE shared_users SET banDay=NULL, banMonth=NULL, banYear=NULL, banReason=NULL WHERE number=$dbNumber";
					        $stmt = mysqli_stmt_init($link);

					        if(mysqli_stmt_prepare($stmt, $query)){
					                mysqli_stmt_execute($stmt);
                					mysqli_stmt_close($stmt);
                					unset($query);
							//LOGIN Below:
	                                                $_SESSION['user_number'] = $dbNumber;
	                                                $_SESSION['user_name'] = $userName;
	                                                $_SESSION['load_first'] = $dbLoadFirst;
	                                                $_SESSION['time_zone'] = $dbTimeZone;
							$_SESSION['is_admin_0'] = $dbSharedAdmin;

	                                                //Update Last Login below:
	                                                $query = 'UPDATE shared_users SET lastLogin="' . date("Y F j") . '" WHERE number=' . $dbNumber;
	                                                $stmt = mysqli_stmt_init($link);

	                                                if(mysqli_stmt_prepare($stmt, $query)){
	                                                        mysqli_stmt_execute($stmt);
	                                                        mysqli_stmt_close($stmt);
	                                                        unset($query);
	                                                }else{
	                                                        die("Error!");
	                                                }
	                                                //Update Last Login Above:

	                                                unset($link);
							$message.='Your ban has ended.<br>You have been logged in successfully.<br><a href="./index.php?m=' . $moduleNumber . '">Continue</a>';
	                                                include($root . $modulePath . $themePath . "header.html");
	                                                include($root . $modulePath . $themePath . "message.html");
	                                                include($root . $modulePath . $themePath . "footer.html");
							//LOGIN Above:
						}else{
							die("Error!");
						}

					}else{
						if((date("Y")==$dbBanYear) && (date("n")>$dbBanMonth)){
							$query = "UPDATE shared_users SET banDay=NULL, banMonth=NULL, banYear=NULL, banReason=NULL WHERE number=$dbNumber";
                                                	$stmt = mysqli_stmt_init($link);

	                                                if(mysqli_stmt_prepare($stmt, $query)){
	                                                        mysqli_stmt_execute($stmt);
	                                                        mysqli_stmt_close($stmt);
	                                                        unset($query);

								//LOGIN Below:
		                                                $_SESSION['user_number'] = $dbNumber;
		                                                $_SESSION['user_name'] = $userName;
		                                                $_SESSION['load_first'] = $dbLoadFirst;
		                                                $_SESSION['time_zone'] = $dbTimeZone;
								$_SESSION['is_admin_0'] = $dbSharedAdmin;

		                                                //Update Last Login below:
		                                                $query = 'UPDATE shared_users SET lastLogin="' . date("Y F j") . '" WHERE number=' . $dbNumber;
		                                                $stmt = mysqli_stmt_init($link);

		                                                if(mysqli_stmt_prepare($stmt, $query)){
		                                                        mysqli_stmt_execute($stmt);
		                                                        mysqli_stmt_close($stmt);
		                                                        unset($query);
		                                                }else{
		                                                        die("Error!");
		                                                }
		                                                //Update Last Login Above:

		                                        	unset($link);
								$message.='Your ban has ended.<br>You have been logged in successfully.<br><a href="./index.php?m=' . $moduleNumber . '">Continue</a>';
			                                       	include($root . $modulePath . $themePath . "header.html");
		        	                                include($root . $modulePath . $themePath . "message.html");
			                                        include($root . $modulePath . $themePath . "footer.html");
								//LOGIN Above:

	                                                }else{
	                                                        die("Error!");
	                                                }
						}else{
							if((date("Y")==$dbBanYear) && (date("n")==$dbBanMonth) && (date("j")>$dbBanDay)){
								$query = "UPDATE shared_users SET banDay=NULL, banMonth=NULL, banYear=NULL, banReason=NULL WHERE number=$dbNumber";
                                                		$stmt = mysqli_stmt_init($link);

		                                                if(mysqli_stmt_prepare($stmt, $query)){
		                                                        mysqli_stmt_execute($stmt);
		                                                        mysqli_stmt_close($stmt);
		                                                        unset($query);
									//LOGIN Below:
			                                                $_SESSION['user_number'] = $dbNumber;
			                                                $_SESSION['user_name'] = $userName;
			                                                $_SESSION['load_first'] = $dbLoadFirst;
			                                                $_SESSION['time_zone'] = $dbTimeZone;
									$_SESSION['is_admin_0'] = $dbSharedAdmin;

			                                                //Update Last Login below:
			                                                $query = 'UPDATE shared_users SET lastLogin="' . date("Y F j") . '" WHERE number=' . $dbNumber;
			                                                $stmt = mysqli_stmt_init($link);

			                                                if(mysqli_stmt_prepare($stmt, $query)){
			                                                        mysqli_stmt_execute($stmt);
			                                                        mysqli_stmt_close($stmt);
			                                                        unset($query);
			                                                }else{
			                                                        die("Error!");
			                                                }
			                                                //Update Last Login Above:

			                                                unset($link);
									$message.='Your ban has ended.<br>You have been logged in successfully.<br><a href="./index.php?m=' . $moduleNumber . '">Continue</a>';
			                                                include($root . $modulePath . $themePath . "header.html");
			                                                include($root . $modulePath . $themePath . "message.html");
			                                                include($root . $modulePath . $themePath . "footer.html");
									//LOGIN Above:
		                                                }else{
		                                                        die("Error!");
		                                                }
							}else{
								unset($link);
								$message.='Your account has been banned until:<br>Year: ' . $dbBanYear . '<br>Month: ' . $dbBanMonth . '<br>Day: ' . $dbBanDay . '<br>Ban Reason:<br>' . $dbBanReason;
                                        			include($root . $modulePath . $themePath . "header.html");
			                                        include($root . $modulePath . $themePath . "message.html");
			                                        include($root . $modulePath . $themePath . "footer.html");
							}
						}
					}

				}
			}else{ //if user's email hasn't been verified
				unset($link); unset($password); unset($dbPassword);
				$message.='Your email address hasn\'t been verified yet.<br>Contact an admin if there is a problem.';
                                include($root . $modulePath . $themePath . "header.html");
                             	include($root . $modulePath . $themePath . "message.html");
                                include($root . $modulePath . $themePath . "footer.html");
			}
		}else{ //if password is wrong:
			unset($password);
			$message.='Login Failed!';
			include($root . $modulePath . $themePath . "header.html");
			include($root . $modulePath . $themePath . "login.html");
			include($root . $modulePath . $themePath . "footer.html");
		}
	}else{//if prepare fails:
		die("Error Logging In!");
	}


	}else{ //if we are NOT logging in (aka POST data NOT sent):
	include($root . $modulePath . $themePath . "header.html");
	include($root . $modulePath . $themePath . "login.html");
	include($root . $modulePath . $themePath . "footer.html");
	}
}

?>
