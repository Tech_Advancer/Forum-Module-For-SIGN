<?php
////////////////////////
//
//  admin_catEdit.php
//  Included by module.php
//  Admin Only
//  Edits categories for
//  the forum module.
////////////////////////


if(((isset($_SESSION['forum_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['forum_' . $moduleNumber . '_admin_2'])) && ($_SESSION['forum_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['forum_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_GET['p'])) && (is_numeric($_GET['p']))){
	$catNumber = $_GET['p'];
}else{
	if((isset($_POST['catNumber'])) && (is_numeric($_POST['catNumber']))){
		$catNumber = $_POST['catNumber'];
	}else{
		die("Error!");
	}
}

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$updated =false;

if((isset($_POST['catName'])) && (isset($_POST['catDes']))){

	$catName = $_POST['catName'];
	$catDes = $_POST['catDes'];

	$query = 'UPDATE forum_' . $moduleNumber . '_categories SET name=?,description=? WHERE number=' . $catNumber;
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ss", $catName, $catDes);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
	$updated = true;
}

//Get category info:
$query = 'SELECT name,description FROM forum_' . $moduleNumber . '_categories WHERE number=' . $catNumber;
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
                $catName = $row->name;
		$catDes = $row->description;
        }
}else{
	die("Error!<br>" . $query);
}
unset($query); unset($row); unset($result);
mysqli_close($link);

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Edit (<?php echo $catNumber . ") " . $catName; ?></h1>

<?php
if($updated){ //if updated
?>
<h3>Updated!</h3>
<?php
} //end if updated
?>
<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=4" method="POST">
<input type="hidden" name="catNumber" value="<?php echo $catNumber; ?>">
<label>Name: <input type="text" name="catName" value="<?php echo $catName; ?>"></label><br><br>

<label>Description:<br>
<textarea rows="15" cols="50" name="catDes"><?php echo $catDes; ?></textarea></label><br><br><br>
<input type="submit" value="Edit">
</form>

<br><br>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&cat=<?php echo $catNumber; ?>">See Category ></a>
<br><br>

<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website
?>
