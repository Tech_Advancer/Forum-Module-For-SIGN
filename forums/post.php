<?php
////////////////////////////
//
//  post.php
//  Included by module.php
//  Loads the page that
//    displays posts for
//    a certain thread.
//  Expects $postNumber[0]
///////////////////////////

$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get first post that started thread
$query = 'SELECT name,content,linkNumber,author,date FROM forum_' . $moduleNumber . '_posts WHERE number=' . $postNumber[0] . ' AND isReply=0';
$query = mysqli_real_escape_string($link, $query);
if($result = mysqli_query($link, $query)){
	$row = mysqli_fetch_assoc($result);
	$postName = $row['name'];
	$postContent[0] = $row['content'];
	$categoryNumber = $row['linkNumber'];
	$postAuthorNumber[0] = $row['author'];
	$postDate[0] = $row['date'];
}else{
	die("Error!");
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

//Translate the $categoryNumber to its name
$query = 'SELECT name FROM forum_' . $moduleNumber . '_categories WHERE number=' . $categoryNumber;
$query = mysqli_real_escape_string($link, $query);
if($result = mysqli_query($link, $query)){
	$row = mysqli_fetch_assoc($result);
	$categoryName = $row['name'];
}else{
	die("Error!");
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

//Translate $postAuthorNumber to username
$query = 'SELECT username FROM shared_users WHERE number=' . $postAuthorNumber[0];
if($result = mysqli_query($link, $query)){
	$row = mysqli_fetch_assoc($result);
	$postAuthor[0] = $row['username'];
}else{
	die("Error!");
}
mysqli_free_result($result); unset($query); unset($row); unset($result);


if((isset($_POST['reply'])) && (strlen(trim($_POST['reply']))>1) && ($_SESSION['user_number']>=0)){ //if someone is sending a reply
	$thisReplyContent = db_safe(trim($_POST['reply']), $link);
	$query = 'INSERT INTO forum_' . $moduleNumber . '_posts (name,content,linkNumber,author,date,isReply) VALUES (?,?,?,?,?,1)';
        $stmt = mysqli_stmt_init($link);
	$thisReplyDate = date("F j, Y");
 	if(mysqli_stmt_prepare($stmt, $query)){
    		mysqli_stmt_bind_param($stmt, "ssiis", $postName, $thisReplyContent , $postNumber[0], $_SESSION['user_number'], $thisReplyDate );
               	mysqli_stmt_execute($stmt);
              	mysqli_stmt_close($stmt);
               	unset($query);
  	}else{ //if stmt_prepare fails:
             	die("Error!");
    	}
}

//Get all replies to main thread
$postCount = 1; //start at one since starting post above is 0
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT number,content,author,date FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $postNumber[0] . ' AND isReply=1';
$query = mysqli_real_escape_string($link, $query);

if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
		$postNumber[$postCount] = $row->number;
        	$postContent[$postCount] = $row->content;
		$postAuthorNumber[$postCount] = $row->author;
				//Translate $postAuthorNumber[$postCount] to username
				$subQuery = 'SELECT username FROM shared_users WHERE number=' . $postAuthorNumber[$postCount];
				if($subResult = mysqli_query($link, $subQuery)){
					$subRow = mysqli_fetch_assoc($subResult);
					$postAuthor[$postCount] = $subRow['username'];
				}else{
					die("Error!");
				}
				mysqli_free_result($subResult); unset($subQuery); unset($subRow); unset($subResult);
        $postDate[$postCount] = $row->date;
        $postCount = $postCount + 1;
        }
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

mysqli_close($link);

include($root . $modulePath . $themePath . 'header.html');
include($root . $modulePath . $themePath . 'post.html');
include($root . $modulePath . $themePath . 'footer.html');

?>
