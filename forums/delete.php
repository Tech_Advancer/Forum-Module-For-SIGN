<?php
/////////////////////////////
//
//  delete.php
//  Can be included by any script
//  Deletes Comments
//  Expects $_GET['p'] to be
//    the post number or
//    $postNumber set by
//    another script.
/////////////////////////////

$link = db_connect($database_url, $database_username, $database_password, $database_name);

if((!isset($auto)) || ($auto=FALSE)){ //if another script is calling this
	if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ //if postNumber isset
		$postNumber = trim($_GET['p']);
	}else{
		die("Error!");
	}
}//end if another script is calling this

if($userRank>0){ //if we are an admin or mod
	//change database variables since other scripts call this
        $deleteLink = db_connect($database_url, $database_username, $database_password, $database_name);

       	//Delete any replies to this post if this post is a Thread Starter
       	$deleteQuery = 'DELETE FROM forum_' . $moduleNumber . '_posts WHERE isReply=1 AND linkNumber=' . $postNumber; //isReply=1 is key here
      	$deleteQuery = mysqli_real_escape_string($link, $deleteQuery);
    	if(!mysqli_query($link, $deleteQuery)){
        	die("Error ");
     	}
        unset($deleteQuery);
	//end deleteing any replies to this post if this post is a Thread Starter

	//Delete the Post:
	$deleteQuery = 'DELETE FROM forum_' . $moduleNumber . '_posts WHERE number=' . $postNumber;
	$deleteQuery = mysqli_real_escape_string($deleteLink, $deleteQuery);
	if(mysqli_query($deleteLink, $deleteQuery)){
		unset($deleteQuery);
		if((!isset($auto)) || ($auto=FALSE)){ //if another script is calling this, hide the confirm message
			mysqli_close($deleteLink);
			$message = 'Post Deleted.<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>';
			include($root . $modulePath . $themePath . "header.html");
			include($root . $modulePath . $themePath . "message.html");
			include($root . $modulePath . $themePath . "footer.html");
		}
	}else{
	        die("Error ");
	}

}//end if we are an admin or mod

?>
