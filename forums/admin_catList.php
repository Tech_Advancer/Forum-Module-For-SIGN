<?php
////////////////////////
//
//  admin_catList.php
//  Included by module.php
//  Admin Only
//  Lists all categories
//  on the forum module.
////////////////////////


if(((isset($_SESSION['forum_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['forum_' . $moduleNumber . '_admin_2'])) && ($_SESSION['forum_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['forum_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Category List</h1>

<table style="width: 95%; margin: auto; border-collapse: collapse;">
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get all of the categories:
$query = 'SELECT number,name FROM forum_' . $moduleNumber . '_categories';
$query = mysqli_real_escape_string($link, $query);
?>
<tr style="background-color: rgb(181,181,181);"><th width="70%;">Category</th><th width="30%;">Settings</th></tr>
<?php
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
?>
<tr><td>
                <a href="./index.php?m=<?php echo $moduleNumber; ?>&cat=<?php echo $row->number; ?>">(<?php echo $row->number . ") &nbsp;" . $row->name; ?></a>
</td><td>
		<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=4&p=<?php echo $row->number; ?>">Edit</a>&nbsp;|&nbsp;
		<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=6&p=<?php echo $row->number; ?>">Delete</a>
</td></tr>
<?php
	}
}
unset($query); unset($row); unset($result);
mysqli_close($link);
?>
</table>

<?php
include('.' . $modulePath . 'admin_footer.php');

} //if you are an admin of the website

?>
