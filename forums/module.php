<?php
//////////////////////////////////
//
//  module.php
//  Loaded from index.php on root.
//  Determines what the user wants
//    based on GET data and includes
//    the appropriate files.
//////////////////////////////////

require_once('installation_variables.php');

//Get the $themePath for this installation (below):
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT textValue FROM forum_' . $moduleNumber . '_settings WHERE name="theme"';

if($resultLink = mysqli_query($link, $query)){
$result = mysqli_fetch_assoc($resultLink);
$themePath='themes/' . $result['textValue'] . '/';
}

mysqli_free_result($resultLink);
unset($query); unset($result);
//Get the $themePath for this installation (above)

//Get links from forum_(module#)_settings below:
$query = 'SELECT textValue FROM forum_' . $moduleNumber . '_settings WHERE name="links"';

if($resultLink = mysqli_query($link, $query)){
$result = mysqli_fetch_assoc($resultLink);
$pageLinks = $result['textValue'];

mysqli_free_result($resultLink);
unset($result); unset($resultLink);
}else{
die("Error!");
}
unset($query);
mysqli_close($link);
//Get links from website_(module#)_settings above

if((isset($_GET['post'])) && (is_numeric(trim($_GET['post'])))){ //if we are loading a post
	$postNumber[0] = trim($_GET['post']); //$postNumber is an array for use later in post.php
	require_once('post.php');
}else{ //if we aren't loading a post
	if((isset($_GET['cat'])) && (is_numeric(trim($_GET['cat'])))){ //if we are loading a category
		$catNumber = trim($_GET['cat']);
		require_once('category.php');
	}else{ //if we aren't loading a category; load index
	        if((isset($_GET['e'])) && (is_numeric(trim($_GET['e'])))){ //if we are loading an extra page (join/login/etc...)
	                $extraNumber=trim($_GET['e']);
	                switch($extraNumber){
	                case 1: require_once('join.php'); break; //Join
	                case 2: require_once('login.php'); break; //Login
	                case 3: require_once('logout.php'); break; //Logout
	                case 4: require_once('activateAccount.php'); break; //Activate Accunt
	                case 5: require_once('ban.php'); break; //Ban User
	                case 6: require_once('delete.php'); break; //Delete a Comment
	                default: require_once('login.php');
	                }
	        }else{  //if we aren't loading an extra page
			if((isset($_GET['a']))){
			//if we are loading the admin panel

				//Double check userRank!
                                $link = db_connect($database_url, $database_username, $database_password, $database_name);
                                $query = 'SELECT rank FROM forum_' . $moduleNumber . '_users WHERE number=' . $_SESSION['user_number'];
                                if($resultLink = mysqli_query($link, $query)){
                                        $result = mysqli_fetch_assoc($resultLink);
                                        $userRank=$result['rank']; //Should NOT be a session variable; We don't want this to follow users around!
                                        $_SESSION['forum_' . $moduleNumber . '_admin_1'] = $userRank;
                                }
                                mysqli_free_result($resultLink);
                                mysqli_close($link);
                                unset($query); unset($result); unset($link);
                                //Double check userRank!

                                if((isset($_SESSION['forum_' . $moduleNumber . '_admin_2'])) && ($_SESSION['forum_' . $moduleNumber . '_admin_1']>2) && ($_SESSION['forum_' . $moduleNumber . '_admin_2']==$userRank)){
                                        //if user has gone through the admin login
                                        switch(trim($_GET['a'])){
						case 0: include('.' . $modulePath . 'admin_logout.php'); break;
                                                case 1: include('.' . $modulePath . 'admin_settings.php'); break;
                                                case 2: include('.' . $modulePath . 'admin_userList.php'); break;
                                                case 3: include('.' . $modulePath . 'admin_catList.php'); break;
                                                case 4: include('.' . $modulePath . 'admin_catEdit.php'); break;
                                                case 5: include('.' . $modulePath . 'admin_catCreate.php'); break;
                                                case 6: include('.' . $modulePath . 'admin_catDelete.php'); break;
                                                case 7: include('.' . $modulePath . 'admin_links.php'); break;
                                                case 8: include('.' . $modulePath . 'admin_userEditor.php'); break;
						case 9: include('.' . $modulePath . 'admin_theme.php'); break;
                                                default: include('.' . $modulePath . 'admin_logout.php'); break;
                                        }
                                }else{
                                        include('.' . $modulePath . 'admin_login.php');
                                }
			}else{
			//if we aren't loading the admin panel
				require_once('forumIndex.php');
			}
		}
	} //end load index
}//end if we aren't loading a post
?>
