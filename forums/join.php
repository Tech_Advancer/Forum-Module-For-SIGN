<?php
///////////////////////////////
//
//  join.php
//  Included by module.php
//////////////////////////////

$message='';

if($_SESSION['user_number']!=-1){ //if user is already logged in
	include($root . $modulePath . $themePath . "header.html");
        $message.='You are already logged in!<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>'; //change this later for a language pack
        include($root . $modulePath . $themePath . "message.html");
        include($root . $modulePath . $themePath . "footer.html");
}else{ //if we aren't logged in
	if(isset($_POST['submit'])){ //if post data was sent

	        $link = db_connect($database_url,$database_username,$database_password,$database_name);

		if(mysqli_connect_errno()){ die("Error"); }

	        $userName = db_safe($_POST['userName'], $link);
	        $password = db_safe($_POST['password'], $link);
		$passwordVerify = db_safe($_POST['passwordVerify']);
		$email = db_safe($_POST['emailAddress']);

		if((strlen($userName)<3) || (strlen($userName)>25)){ $message.='Your username MUST be between 3 and 25 characters long!<br>'; }
                if((strlen($password)<8) || (strlen($password)>70)){ $message.='Your password MUST be between 8 and 70 characters long!<br>'; }
                if(strcmp($password, $passwordVerify) !== 0){ $message.='Your password didn\'t match! It IS case sensitive!<br>'; }
                if(strlen($email)<3){ $message.='You must enter your email address!<br>'; }

		if(strlen($message)<1){ //if there isn't some other error already
			//query to be sure the username/email isn't already being used
			$query = "SELECT count(username) FROM shared_users WHERE username=? OR emailAddress=?";
	                $stmt = mysqli_stmt_init($link);

	                if(mysqli_stmt_prepare($stmt, $query)){
	                	mysqli_stmt_bind_param($stmt, "ss", $userName, $email);
	                        mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				mysqli_stmt_bind_result($stmt, $conflicts);
				mysqli_stmt_fetch($stmt);
	                        mysqli_stmt_close($stmt);
	                        unset($query);
	             	}else{ //if stmt_prepare fails:
	                     	die("Error!");
	            	}
		}//end if there isn't some other error already

		if((isset($conflicts)) && ($conflicts>0)){ $message.='That username or email address is already being used. Please use another.<br>'; }

		if(strlen($message)>1){ //if there is an error
			unset($password); unset($passwordVerify);
			include($root . $modulePath . $themePath . "header.html");
			include($root . $modulePath . $themePath . "join.html");
			include($root . $modulePath . $themePath . "footer.html");
		}else{ //if there is no error
		//hash password
		$password = password_hash($password, PASSWORD_DEFAULT, $hashOptions['options']);
		$joinDate = date("F j, Y");
		$aac = sha1($userName . mt_rand(0,10000) . $joinDate);
		$uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $modulePath . $userName);
		$ip = get_ip();

		$subject = "Thank you for joining " . $moduleName;
                $body = "Thank you for joining " . $moduleName . ", " . $userName . "! To verify your account, please click the link below:<br>";
                $body .= "<a href=\"" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?m=" . $moduleNumber . "&e=4&aac=" . $aac . "\">" .
$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?m=" . $moduleNumber . "&e=4&aac=" . $aac . "</a>";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                $issent = mail($email, $subject, $body, $headers);

		if(!$issent){ die("Error Sending Email Activation!"); }

		//Insert into shared_users database
		$query = "INSERT INTO shared_users (username, password, emailAddress, joinDate, sharedAdmin, loadFirst, accountActivationCode, ipList, uniqueID) VALUES (?,?,?,?,0,?,?,?,?)";
		$stmt = mysqli_stmt_init($link);

		        if(mysqli_stmt_prepare($stmt, $query)){
			        mysqli_stmt_bind_param($stmt, "ssssssss", $userName, $password, $email, $joinDate, $moduleNumber, $aac, $ip, $uid);
			        mysqli_stmt_execute($stmt);
		                mysqli_stmt_close($stmt);
		                unset($query);
			}else{ //if stmt_prepare fails:
				die("Error!");
			}
		//Get user's number
                $query = "SELECT number FROM shared_users WHERE uniqueID='" . $uid . "'";
                if($result = mysqli_query($link, $query)){
                      	$row = mysqli_fetch_assoc($result);
			$userNum = $row['number'];
			unset($row); unset($result); unset($query);
                }else{
                        die("Error Joining!"); //probably should drop the user from the shared table here, but it is fine for now
                }

		//Get all installation paths so we can insert user data into them
		$query = "SELECT number,location FROM shared_installations";
		if($result = mysqli_query($link, $query)){
			while($row = mysqli_fetch_object($result)){
				$subModuleNumber = $row->number;
				include($root . $row->location . 'moduleJoin.php');
			}
			unset($row); unset($result); unset($query);
		}else{
			die("Error Joining!"); //probably should drop the user from the shared table here, but it is fine for now
		}

			unset($password); unset($passwordVerify); unset($aac); unset($uid); unset($ip);
			$message.='Successfully Joined!<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>';;
			include($root . $modulePath . $themePath . "header.html");
                        include($root . $modulePath . $themePath . "message.html");
                        include($root . $modulePath . $themePath . "footer.html");
		} //end if there is no error
	}else{ //if post data wasn't sent:
		include($root . $modulePath . $themePath . "header.html");
                include($root . $modulePath . $themePath . "join.html");
                include($root . $modulePath . $themePath . "footer.html");
	} //end if post wasn't sent
} //end if we aren't logged in

?>
