<?php
///////////////////////////
//
//  forumIndex.php
//  Included by module.php
//  Loads the forums category list.
///////////////////////////

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT number,name,description FROM forum_' . $moduleNumber . '_categories';
$query = mysqli_real_escape_string($link, $query);
$catCount = 0;

if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
        	$catNumber[$catCount] = $row->number;
                $catName[$catCount] = $row->name;
                $catDesc[$catCount] = $row->description;
                $catCount = $catCount + 1;
     	}
}

include($root . $modulePath . $themePath . 'header.html');
include($root . $modulePath . $themePath . 'forumIndex.html');
include($root . $modulePath . $themePath . 'footer.html');

?>
