<?php
//////////////////////////////
//
//  category.php
//  Included by module.php
//  Loads the page that shows
//    the posts inside of a
//    category.
//  Expects $catNumber;
/////////////////////////////


$link = db_connect($database_url, $database_username, $database_password, $database_name);

if((isset($_POST['newThreadContent'])) && (strlen(trim($_POST['newThreadTitle']))>1) && (strlen(trim($_POST['newThreadContent']))>1) && ($_SESSION['user_number']>=0)){
//Insert new thread into db
	$thisThreadTitle = db_safe(trim($_POST['newThreadTitle']), $link);
	$thisThreadContent = db_safe(trim($_POST['newThreadContent']), $link);
        $query = 'INSERT INTO forum_' . $moduleNumber . '_posts (name,content,linkNumber,author,date,isReply) VALUES (?,?,?,?,?,0)';
        $stmt = mysqli_stmt_init($link);
        $thisThreadDate = date("F j, Y");
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ssiis", $thisThreadTitle, $thisThreadContent, $catNumber, $_SESSION['user_number'], $thisThreadDate );
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
}
unset($query);

//Get posts for category #($catNumber) and put into a usable array://Get posts for category #($catNumber) and put into a usable array:
$query = 'SELECT number,name,date,author FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $catNumber . ' AND isReply=0';
$query = mysqli_real_escape_string($link, $query);

$postCount = 0;

if($result = mysqli_query($link, $query)){
  	while($row = mysqli_fetch_object($result)){
         	$postNumber[$postCount] = $row->number;
			//get last post from this thread

			$subQuery = 'SELECT number,author,date FROM forum_' . $moduleNumber . '_posts WHERE number=(SELECT MAX(number) FROM forum_' . $moduleNumber . '_posts WHERE linkNumber=' . $postNumber[$postCount] . ' AND isReply=1)';
			$subQuery = mysqli_real_escape_string($link, $subQuery);
                        if($subResult = mysqli_query($link, $subQuery)){
                                $subRow = mysqli_fetch_assoc($subResult);
                                $lastPostAuthorNumber[$postCount] = $subRow['author'];
				$lastPostNumber[$postCount] = $subRow['number'];
				$lastPostDate[$postCount] = $subRow['date'];
				//if database actually grabs non-empty values
				if((is_numeric($lastPostAuthorNumber[$postCount])) && (is_numeric($lastPostNumber[$postCount])))
				{
					//get Author name from the number
		                        $subsubQuery = 'SELECT username FROM shared_users WHERE number=' . $lastPostAuthorNumber[$postCount];
		                        $subsubQuery = mysqli_real_escape_string($link, $subsubQuery);
		                        if($subsubResult = mysqli_query($link, $subsubQuery)){
		                                $subsubRow = mysqli_fetch_assoc($subsubResult);
		                                $lastPostAuthor[$postCount] = $subsubRow['username'];
		                        }else{
		                                die("Error!");
		                        }
		                mysqli_free_result($subsubResult); unset($subsubQuery); unset($subsubRow); unset($subsubResult);
				}else{
				//if empty values given
				$lastPostAuthor[$postCount] = "None";
				$lastPostNumber[$postCount] = $postNumber[$postCount];
				$lastPostDate[$postCount] = "";
				$lastPostAuthorNumber[$postCount] = -1;
				}
                        }else{
                                die("Error!");
                        }
                        mysqli_free_result($subResult); unset($subQuery); unset($subRow); unset($subResult);

                $postName[$postCount] = $row->name;
                $postDate[$postCount] = $row->date;
                $postAuthorNum[$postCount] = $row->author;
			//get Author name from the post number
			$subQuery = 'SELECT username FROM shared_users WHERE number=' . $postAuthorNum[$postCount];
			$subQuery = mysqli_real_escape_string($link, $subQuery);
			if($subResult = mysqli_query($link, $subQuery)){
				$subRow = mysqli_fetch_assoc($subResult);
				$postAuthor[$postCount] = $subRow['username'];
			}else{
				die("Error!");
			}
			mysqli_free_result($subResult); unset($subQuery); unset($subRow); unset($subResult);

                $postCount = $postCount + 1;
     	}
}
mysqli_free_result($result); unset($result); unset($row); unset($query);


$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT number,name FROM forum_' . $moduleNumber . '_categories WHERE number=' . $catNumber;
$query = mysqli_real_escape_string($link, $query);
if($result = mysqli_query($link, $query)){
	$row = mysqli_fetch_assoc($result);
	$dbCatNumber = $row['number']; //to be sure the category even exists
      	$catName = $row['name'];
}else{
  	die("Error!");
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

mysqli_close($link);

include($root . $modulePath . $themePath . 'header.html');
if($dbCatNumber == $catNumber){
include($root . $modulePath . $themePath . 'category.html');
}else{
echo "<h2>Category Not Found.</h2>";
}
include($root . $modulePath . $themePath . 'footer.html');

?>
